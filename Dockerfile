FROM debian:bookworm-slim AS exporter_build

WORKDIR /tmp
RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends curl ca-certificates

RUN MEMCACHED_EXPORTER_VERSION=0.10.0 && \
    curl -vsfL https://github.com/prometheus/memcached_exporter/releases/download/v${MEMCACHED_EXPORTER_VERSION}/memcached_exporter-${MEMCACHED_EXPORTER_VERSION}.linux-amd64.tar.gz | tar xzf - && \
    cp memcached_exporter-${MEMCACHED_EXPORTER_VERSION}.linux-amd64/memcached_exporter /usr/bin/memcached_exporter && \
    rm -fr memcached_exporter*

FROM registry.git.autistici.org/ai3/docker/s6-overlay-lite:master

RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends memcached && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/*

COPY --from=exporter_build /usr/bin/memcached_exporter /usr/bin/memcached_exporter
COPY services.d/ /etc/services.d/

