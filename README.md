Simple Docker role for memcached.

The server can be configured with environment variables:

`MEM` is the max cache size in MB (default 64)

`PORT` is the port to listen on (default 11211)

`ENABLE_SASL` can be set to any value to enable SASL authentication
(a valid */etc/sasldb2* should be mounted into the container for it
to work).

The container will run a Prometheus exporter on PORT + 1, this can
be overridden with the `EXPORTER_PORT` environment variable.

