#!/bin/sh

target=127.0.0.1
port=11211

ret=0
for dir in testdata/*; do
    netcat-expect $dir $target $port
    if [ $? -ne 0 ]; then
        echo "FAILURE"
        ret=1
    fi
done

exit $ret
